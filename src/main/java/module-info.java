module ntnu.idatt2001.wargames {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    opens ntnu.idatt2001.wargames.Scenes to javafx.fxml;
    exports ntnu.idatt2001.wargames;
}
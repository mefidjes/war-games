package ntnu.idatt2001.wargames.IO;

import ntnu.idatt2001.wargames.Battle.Army;
import ntnu.idatt2001.wargames.Units.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class ArmyWriter {
    private static final String DELIMITER = ",";
    private static final String NEWLINE = "\n";

    ArmyWriter() {}

    /**
     * The write army to file methods checks to make sure the file it is writing to ends in .csv, that the army
     * is not null and that the path is in the correct place. After this it will take the name of the army as
     * the first line of the document and try to write every unit as each subsequent line in the file. If a unit is
     * invalid it will throw an IOException to the user.
     * @param army Takes in an army from which it takes the values
     * @param file File to which write the army into
     * @throws IOException
     */
    public static void writeArmyToFile(Army army, File file) throws IOException {

        if (!file.getName().endsWith(".csv")){
            throw new IOException("All files must end with csv");
        }
        if (army == null) {
            throw new IOException("Army can not be null");
        }
        if (!file.getPath().startsWith(FileSystems.getDefault().getPath("src", "main", "resources").toString())) {
            throw new IOException("Files must be written to src/main/resources.");
        }
        String header = army.getName();
        try (FileWriter fileWriter = new FileWriter(file)){
            fileWriter.write(header + NEWLINE);
            ArrayList<String> armyAsList = shortenArmy(army);
            for (String line : armyAsList){
                try {
                    fileWriter.write(line + NEWLINE);
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        } catch (IOException e){
            throw new IOException("Error writing army to file, error message: " + e.getMessage());
        }
    }

    /**
     * This method takes in an army and will return a arraylist of that army but instead of allowing 10 copies
     * of one unit to represent 10 objects, it will instead be one object with a 10. This is to drastically
     * reduce the storage required for larger armies.
     * @param army Takes in an army
     * @return Returns an arraylist of an army
     */
    private static ArrayList<String> shortenArmy(Army army){
        ArrayList<String> shortenedArmy = new ArrayList<>();
        boolean unitFound = false;

        for (Unit unit : army.getAllUnits()){
            Unit currentUnit = unit;
            String[] unitData = unitToData(currentUnit);

            for (int i = 0; i < shortenedArmy.size(); i++) {
                String[] fileLineData = fileLineToData(shortenedArmy.get(i));
                if (unitData[0].equals(fileLineData[0]) && unitData[1].equals(fileLineData[1]) && unitData[2].equals(fileLineData[2])) {
                    shortenedArmy.set(i, unitData[0] + DELIMITER + unitData[1] + DELIMITER + unitData[2] + DELIMITER + Integer.toString((Integer.parseInt(fileLineData[3])+1))); //Comeback here please
                    unitFound = true;
                }
            }
            if (!unitFound) {
                shortenedArmy.add(unitData[0] + DELIMITER + unitData[1] + DELIMITER + unitData[2] + DELIMITER + 1);
            }
            unitFound = false;
        }
        return shortenedArmy;
    }

    /**
     * This converts a unit into a array of strings so that each data point of the unit is easily available in string form
     * @param unit Takes in a unit
     * @return Returns a String array
     */
    private static String[] unitToData(Unit unit){
        return new String[]{unit.getUnitType().toString(), unit.getName(), Integer.toString(unit.getHealth())};
    }

    /**
     * Takes in a file line and converts it into the same data object as the unitToData method, essential for comparing
     * armies to the corresponding file.
     * @param fileLine Takes in a string of a file line
     * @return Returns a string array
     */
    private static String[] fileLineToData(String fileLine){
        return fileLine.split(DELIMITER);
    }

    /**
     * Method to create a new army and to add it to the listOfArmies document to index it.
     * @param army Takes in an army
     * @throws IOException If there is an invalid character
     */
    public static void createNewArmyFile(Army army) throws IOException {
        FileWriter fileWriter = new FileWriter(ArmyReader.url + "listOfArmies.csv", true);
        fileWriter.write(army.getName()+NEWLINE);
        fileWriter.flush();
        fileWriter.close();
        writeArmyToFile(army, new File(ArmyReader.url + army.getName() + ".csv"));
    }

    /**
     * Method to remove an army from the list of armies file and to delete the corresponding file
     * Method heavily inspired by https://stackoverflow.com/questions/1377279/find-a-line-in-a-file-and-remove-it
     * @param name
     */
    public static void deleteArmyFile(String name) throws IOException {
        File file = new File(ArmyReader.url + "listOfArmies.csv");
        List<String> out = Files.lines(file.toPath())
                .filter(line -> !line.contains(name))
                .collect(Collectors.toList());
        Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);

        File fileToBeDeleted = new File(ArmyReader.url + name + ".csv");
        fileToBeDeleted.delete();
    }

}

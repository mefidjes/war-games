package ntnu.idatt2001.wargames.IO;

import ntnu.idatt2001.wargames.Battle.Army;
import ntnu.idatt2001.wargames.Units.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Follow code is heavily inspired by the example code of idatt2001 from a lecture from ntnu.
 * The code has been modified and adapted to suit the specific army classes.
 */
public class ArmyReader {
    private static final String DELIMITER = ",";
    public static final String url = "src/main/resources/ntnu/idatt2001/wargames/armies/";

    public ArmyReader() {}

    /**
     * Reads army from a csv file. First there are checks made to ensure that the file exists,
     * that it is of type csv. If the file is not empty, the first line in the file is the name
     * of the army used in the army constructor. After every line it checks which unit type it is and instantiates
     * a new unit of that type and adds it to the army before returning the army.
     * @param file Takes in a csv file of format (type,name,health)
     * @return Returns an army with all the units inside
     * @throws IOException
     */
    public Army readArmyFromFile(File file) throws IOException {
        if (!file.exists()){
            throw new IOException("File does not exist");
        }
        if (!file.getName().endsWith(".csv")){
            throw new IOException("File must be of type csv");
        }
        Army army;
        try (Scanner reader = new Scanner(file)){
            if (!reader.hasNext()){
                throw new IOException("No line to read, file is empty.");
            }
            army = new Army(reader.nextLine());
            while (reader.hasNext()){
                String nextLine = reader.nextLine();
                String[] data = nextLine.split(DELIMITER);
                if (data.length != 4) {
                    throw new IOException("Incorrect amount of values. (Must be 4)");
                }
                UnitType typeName = UnitType.valueOf(data[0]);
                String unitName = data[1];
                int health = Integer.parseInt(data[2]);
                int amount = Integer.parseInt(data[3]);
                try {
                    army.addAll(UnitFactory.newUnits(typeName, unitName, health, amount));
                } catch (Exception e) {
                    throw new IOException("Error adding units.");
                }

            }
        }
        return army;
    }

    /**
     * Takes each line in a file and adds that as an individual string object in an arraylist. This is for
     * allowing files to be easily converted into armies.
     * @param file Takes in a file
     * @return Returns an arraylist
     * @throws FileNotFoundException Throws FileNotFound if no file is put in.
     */
    public static ArrayList<String> fileToArray(File file) throws FileNotFoundException {
        ArrayList<String> array = new ArrayList<>();
        try (Scanner reader = new Scanner(file)) {
            while (reader.hasNext()) {
                array.add(reader.nextLine());
            }
        }
        return array;
    }
}

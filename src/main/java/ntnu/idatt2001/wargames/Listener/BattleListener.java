package ntnu.idatt2001.wargames.Listener;

/**
 * Inspiration: https://www.youtube.com/watch?v=a2aB9n472U0
 * This class is an interface which allows very specific times for this class to observe another class and
 * have it force an update from another class.
 */
public interface BattleListener {
    void onUnitCountChange();
}

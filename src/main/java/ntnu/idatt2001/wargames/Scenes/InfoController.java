package ntnu.idatt2001.wargames.Scenes;

import java.io.IOException;

public class InfoController {

    /**
     * Switches the current scene to the main page
     * @throws IOException
     */
    public void toMainPage() throws IOException {
        ViewSwitcher.switchTo(SceneView.MAIN);
    }
    /**
     * Switches the current scene to the armies page
     * @throws IOException
     */
    public void toArmies() throws IOException {
        ViewSwitcher.switchTo(SceneView.ARMY);
    }
    /**
     * Switches the current scene to the battle page
     * @throws IOException
     */
    public void toBattle() throws IOException {
        ViewSwitcher.switchTo(SceneView.BATTLE);
    }
}

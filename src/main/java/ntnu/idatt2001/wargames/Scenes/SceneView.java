package ntnu.idatt2001.wargames.Scenes;

/**
 * Inspired by Almas Baimagambetov (almaslvl@gmail.com)
 * Stores all the different scenes as an enum with their respective-file names
 * Simplifies switching scenes
 */
public enum SceneView {
    MAIN("main-page.fxml"),
    ARMY("army.fxml"),
    BATTLE("battle.fxml"),
    INFO("info.fxml");

    private String fileName;

    SceneView(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

}

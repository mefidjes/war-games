package ntnu.idatt2001.wargames.Scenes;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import ntnu.idatt2001.wargames.Battle.Army;
import ntnu.idatt2001.wargames.Battle.Battle;
import ntnu.idatt2001.wargames.IO.ArmyReader;
import ntnu.idatt2001.wargames.Listener.BattleListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class BattleController {

    @FXML
    ComboBox<String> armyOneArmySelector = new ComboBox<>();

    @FXML
    ComboBox<String> armyTwoArmySelector = new ComboBox<>();


    //Terrain Selector uses all elements from the terrain enum and adds them to a list
    @FXML
    ComboBox<Battle.Terrain> terrainSelector = new ComboBox<>();

    @FXML
    Text armyOneTotalUnits;

    @FXML
    Text armyTwoTotalUnits;

    @FXML
    Text armyOneInfantryUnits;

    @FXML
    Text armyTwoInfantryUnits;

    @FXML
    Text armyOneRangedUnits;

    @FXML
    Text armyTwoRangedUnits;

    @FXML
    Text armyOneCavalryUnits;

    @FXML
    Text armyTwoCavalryUnits;

    @FXML
    Text armyOneCommanderUnits;

    @FXML
    Text armyTwoCommanderUnits;

    @FXML
    ImageView battleBackground;

    @FXML
    ImageView armyOneCrown;

    @FXML
    ImageView armyTwoCrown;

    @FXML
    Button simulateButton;

    @FXML
    Label warning;


    Army armyOne;
    Army armyTwo;

    Battle battle;

    /**
     * On starting the page sets up all the boxes with the correct options. Since these options never change on this
     * scene there is no need for these to be called again.
     * @throws FileNotFoundException
     */
    @FXML
    public void initialize() throws FileNotFoundException {
        
        //Method takes all elements from the terrain enum and populates the selector
        terrainSelector.setItems(FXCollections.observableArrayList(Battle.Terrain.values()));
        armyOneArmySelector.setItems(FXCollections.observableArrayList(ArmyReader.fileToArray(new File(ArmyReader.url + "listOfArmies.csv"))));
        armyTwoArmySelector.setItems(FXCollections.observableArrayList(ArmyReader.fileToArray(new File(ArmyReader.url + "listOfArmies.csv"))));
    }

    /**
     * Every time the armyOneSelector updates it will read the army and display the number of units underneath.
     * @throws IOException Can't throw this exception since its from a list
     */
    @FXML
    public void onArmyOneSelected() throws IOException {
        ArmyReader reader = new ArmyReader();
        armyOne = reader.readArmyFromFile(new File(ArmyReader.url + armyOneArmySelector.getValue() +".csv"));
        updateArmyOne();
    }
    /**
     * Every time the armyTwoSelector updates it will read the army and display the number of units underneath.
     * @throws IOException Can't throw this exception since its from a list
     */
    @FXML
    public void onArmyTwoSelected() throws IOException {
        ArmyReader reader = new ArmyReader();
        armyTwo = reader.readArmyFromFile(new File(ArmyReader.url + armyTwoArmySelector.getValue() +".csv"));
        updateArmyTwo();
    }

    /**
     * Every time terrain is selected it will update the static terrain variable and the corresponding background.
     */
    @FXML
    public void onTerrainSelected() {
        Battle.setBattleTerrain(terrainSelector.getValue());
        battleBackground.setImage(new Image("file:src/main/resources/ntnu/idatt2001/wargames/images/" + terrainSelector.getValue().getName() + ".png"));
    }

    /**
     * Checks to make sure neither army is null. It creates a new battle and adds a listener to the battle.
     * Starts a new thread which simulates teh battle and then sets a winner. It calles the update armies method
     * which runs later so that the threads don't run over each other.
     */
    public void simulate() {
        if (armyOneArmySelector.getValue() != null && armyTwoArmySelector.getValue() != null) {
        warning.setVisible(false);
        simulateButton.setDisable(true);
        try{
            battle = new Battle(armyOne, armyTwo);
            battle.addListener(this::updateArmies);
            new Thread(()-> {
                battle.simulate();
                setWinner();
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        }
        else {
            warning.setText("Please select an army for each side.");
            warning.setVisible(true);
        }

    }

    /**
     * Updates all the values in armyOne
     */
    public void updateArmyOne(){
        armyOneTotalUnits.setText(Integer.toString(armyOne.getTotalUnitsAmount()));
        armyOneInfantryUnits.setText(Integer.toString(armyOne.getUnitsByClass("InfantryUnit")));
        armyOneRangedUnits.setText(Integer.toString(armyOne.getUnitsByClass("RangedUnit")));
        armyOneCavalryUnits.setText(Integer.toString(armyOne.getUnitsByClass("CavalryUnit")));
        armyOneCommanderUnits.setText(Integer.toString(armyOne.getUnitsByClass("CommanderUnit")));
    }

    /**
     * Updates all the values in armyTwo
     */
    public void updateArmyTwo(){
        armyTwoTotalUnits.setText(Integer.toString(armyTwo.getTotalUnitsAmount()));
        armyTwoInfantryUnits.setText(Integer.toString(armyTwo.getUnitsByClass("InfantryUnit")));
        armyTwoRangedUnits.setText(Integer.toString(armyTwo.getUnitsByClass("RangedUnit")));
        armyTwoCavalryUnits.setText(Integer.toString(armyTwo.getUnitsByClass("CavalryUnit")));
        armyTwoCommanderUnits.setText(Integer.toString(armyTwo.getUnitsByClass("CommanderUnit")));
    }

    /**
     * Calls both update army methods which is constantly reffered to.
     */
    public void updateArmies(){
        Platform.runLater(()-> {
            updateArmyOne();
            updateArmyTwo();});
    }

    /**
     * Sets a crown over the winning army.
     */
    public void setWinner() {
        if (armyOne.hasUnits()) {
            armyOneCrown.setVisible(true);
        }
        if (armyTwo.hasUnits()) {
            armyTwoCrown.setVisible(true);
        }
    }

    /**
     * Resets the battle to the original state and re-enables the simulate button.
     * @throws IOException
     */
    public void reset() throws IOException {
        simulateButton.setDisable(false);
        armyOneCrown.setVisible(false);
        armyTwoCrown.setVisible(false);
        onArmyOneSelected();
        onArmyTwoSelected();
    }
    /**
     * Switches the current scene to the armies page whilst shutting down the thread
     * @throws IOException
     */
    public void toArmies() throws IOException {
        Battle.setShutdownRequest();
        ViewSwitcher.switchTo(SceneView.ARMY);
    }
    /**
     * Switches the current scene to the main page whilst shutting down the thread
     * @throws IOException
     */
    public void toMainPage() throws IOException {
        Battle.setShutdownRequest();
        ViewSwitcher.switchTo(SceneView.MAIN);
    }
}

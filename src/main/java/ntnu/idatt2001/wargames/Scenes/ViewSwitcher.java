package ntnu.idatt2001.wargames.Scenes;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.net.URL;

/**
 * Inspired by Almas Baimagambetov (almaslvl@gmail.com)
 */
public class ViewSwitcher {

    private static Scene scene;

    /**
     * Sets the scene to the input scene
     * @param scene Takes in a scene
     */
    public static void setScene(Scene scene) {
        ViewSwitcher.scene = scene;
    }

    /**
     * Switches the current scene to target scene
     * @param view Takes in an enum of type view
     * @throws IOException if given an incorrect scene
     */
    public static void switchTo(SceneView view) throws IOException {

        if (scene == null) {
            return;
        }

        try {
            Parent root = FXMLLoader.load(new URL("file:src/main/resources/ntnu/idatt2001/wargames/Scenes/" + view.getFileName()));
            scene.setRoot(root);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

package ntnu.idatt2001.wargames.Scenes;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import ntnu.idatt2001.wargames.Battle.Army;
import ntnu.idatt2001.wargames.IO.ArmyReader;
import ntnu.idatt2001.wargames.IO.ArmyWriter;
import ntnu.idatt2001.wargames.Units.Unit;
import ntnu.idatt2001.wargames.Units.UnitFactory;
import ntnu.idatt2001.wargames.Units.UnitType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArmyController {

    @FXML
    ScrollPane armiesList = new ScrollPane();

    @FXML
    ComboBox<UnitType> unitTypeSelector = new ComboBox<UnitType>();

    @FXML
    ComboBox<String> armySelector = new ComboBox<String>();

    @FXML
    TextField nameField;

    @FXML
    TextField healthField;

    @FXML
    TextField amountField;

    @FXML
    Text errorMessage;

    @FXML
    TextField armyNameField;

    VBox armyVBox = new VBox();

    Army army;

    /**
     * Updates all the information on the screen, usually called after a change.
     * @throws FileNotFoundException
     */
    public void update() throws FileNotFoundException {
        armySelector.setItems(FXCollections.observableArrayList(ArmyReader.fileToArray(new File(ArmyReader.url + "listOfArmies.csv"))));
        unitTypeSelector.setItems(FXCollections.observableArrayList(UnitType.values()));
        armiesList.setContent(armyVBox);
    }

    /**
     * Initialize simply updates the screen and disables the error message
     * @throws FileNotFoundException
     */
    @FXML
    public void initialize() throws FileNotFoundException {
        update();
        errorMessage.setVisible(false);
    }

    /**
     * Takes in all the fields filled out by the user and adds it the selected army. Checks to make sure the unit type
     * is not null. Does this by recreating the entire army each time and adding the appropriate units to the file
     */
    @FXML
    public void addUnitToArmy() {
        try {
            if (unitTypeSelector.getValue() == null) {
                errorMessage.setText("Select a unit type.");
                errorMessage.setVisible(true);
            } else {
                ArmyReader reader = new ArmyReader();
                army = reader.readArmyFromFile(new File(ArmyReader.url + armySelector.getValue() + ".csv"));
                army.addAll(UnitFactory.newUnits(unitTypeSelector.getValue(), nameField.getText(), Integer.parseInt(healthField.getText()), Integer.parseInt(amountField.getText())));
                ArmyWriter.writeArmyToFile(army, new File(ArmyReader.url + armySelector.getValue() + ".csv"));
                loadArmy();
            }
        } catch (IllegalArgumentException e){
            errorMessage.setText("Invalid input type.");
            errorMessage.setVisible(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new army by checking if the name is taken, if it isn't it will instantiate a new army and add it to
     * the list of armies document.
     * @throws IOException
     */
    @FXML
    public void createNewArmy() throws IOException {
        String armyName = armyNameField.getText();
        boolean nameTaken = false;
        for (String line : ArmyReader.fileToArray(new File(ArmyReader.url + "listOfArmies.csv"))) {
            if (line.equals(armyName)) {
                nameTaken = true;
                break;
            }
        }
        if (!nameTaken) {
            Army army = new Army(armyName);
            ArmyWriter.createNewArmyFile(army);
            update();
        }
    }

    /**
     * Takes the army from the ComboBox selector and finds the corresponding file. Takes each element from the file and
     * displays it on the screen to allow user to get a general overview of the army. Additionally sets an on mouseclick
     * action for the labels in the list of units, allowing units to be deleted.
     * @throws FileNotFoundException
     */
    @FXML
    public void loadArmy() throws FileNotFoundException {
        armyVBox.getChildren().clear();
        ArrayList<String> lines = ArmyReader.fileToArray(new File(ArmyReader.url + armySelector.getValue() + ".csv"));
        for (int i = 0; i < lines.size() - 1; i ++) {
            Label label = new Label(lines.get(i+1));
            label.setId(Integer.toString(i));
            armyVBox.getChildren().add(label);
            armyVBox.getChildren().get(i).setOnMouseClicked(mouseEvent -> {
                try {
                    deleteUnitsFromArmy(label.getText());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    /**
     * Delets an army from the list of armies file along with the file itself by calling the deleteArmyMethod
     * @throws IOException
     */
    @FXML
    public void deleteArmy() throws IOException {
        try {
            String armyName = armySelector.getValue();
            if (armyName == null) {
                errorMessage.setText("Select an army.");
                errorMessage.setVisible(true);
            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Delete " + armyName + " permanently?", ButtonType.YES, ButtonType.NO);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.YES) {
                    ArmyWriter.deleteArmyFile(armyName);
                }
                update();
            }
        } catch (Exception e) {
            errorMessage.setText("Select an army.");
            errorMessage.setVisible(true);
        }
    }

    /**
     * Deletes a unit from army
     */
    public void deleteUnitsFromArmy(String line) throws IOException {
        String armyName = armySelector.getValue();
        ArmyReader armyReader = new ArmyReader();
        Army army = armyReader.readArmyFromFile(new File(ArmyReader.url + armyName + ".csv"));
        try {
            ArrayList<Unit> unitsToBeRemoved = new ArrayList<>(UnitFactory.newUnits(UnitType.valueOf(line.split(",")[0]), line.split(",")[1], Integer.parseInt(line.split(",")[2]), Integer.parseInt(line.split(",")[3])));
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Delete " + line.split(",")[0].toLowerCase() + " unit, called " + line.split(",")[1] + ", with hp " + line.split(",")[2] + ", permanently?", ButtonType.YES, ButtonType.NO);
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {
                army.removeAll(unitsToBeRemoved);
                ArmyWriter.writeArmyToFile(army, new File(ArmyReader.url + armyName + ".csv"));
                }
            } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Switches the current scene to the main page
     * @throws IOException
     */
    @FXML
    public void toMainPage() throws IOException {
       ViewSwitcher.switchTo(SceneView.MAIN);
    }

    /**
     * Switches the current scene to the battle page
     * @throws IOException
     */
    @FXML
    public void toBattle() throws IOException {
        ViewSwitcher.switchTo(SceneView.BATTLE);
    }

}

package ntnu.idatt2001.wargames.Scenes;

import javafx.fxml.FXML;

import java.io.IOException;

public class MainPageController {

    /**
     * Switches the current scene to the battle page
     * @throws IOException
     */
    @FXML
    public void toBattle() throws IOException {
        ViewSwitcher.switchTo(SceneView.BATTLE);
    }
    /**
     * Switches the current scene to the armies page
     * @throws IOException
     */
    @FXML
    public void toArmies() throws IOException {
        ViewSwitcher.switchTo(SceneView.ARMY);
    }
    /**
     * Switches the current scene to the info page
     * @throws IOException
     */
    @FXML
    public void toInfo() throws IOException {
        ViewSwitcher.switchTo(SceneView.INFO);
    }
}

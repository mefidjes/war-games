package ntnu.idatt2001.wargames.Battle;

import ntnu.idatt2001.wargames.Units.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Army {

    String name;
    public List<Unit> units = new ArrayList<Unit>();

    public Army(String name) {
        this.name = name;
    }

    /**
     * Simple constructor takes in name of army and list of units
     * @param name Name of army
     * @param army list of units
     */
    public Army(String name, List<Unit> army) {
        this.name = name;
        this.units = army;
    }

    /**
     * Simple method to get the name of the army
     */
    public String getName () {
        return name;
    }

    /**
     * Adds a unit to the array of units in the army
     * @param unit Unit
     */
    public void add (Unit unit){
            this.units.add(unit);
        }

    /**
     * Adds multiple units to the array of units in the army
     * @param units Takes in an array of units
     */
        public void addAll (List < Unit > units) {
            this.units.addAll(units);
        }

    /**
     * Removes a unit from the array of units in the army
     * @param unit
     */
        public void remove (Unit unit){
            this.units.remove(unit);
        }

    /**
     * Checks the units in the army for if it contains the unit in the parameter
     * @param unit
     * @return Returns a boolean for wheter said unit is in the army
     */
        public boolean hasUnit (Unit unit){
            return this.units.contains(unit);
        }

    /**
     * Checks to see wheter the army has any units
     * @return returns boolean true if the army contains units
     */
        public boolean hasUnits () {
            return !this.units.isEmpty();
        }

    /**
     * Returns all units from the list as a deep copy by iterating through the units and checking the class
     * @return
     */
        public List<Unit> getAllUnits () {
            ArrayList<Unit> unitList = new ArrayList<>();
            for (Unit unit : this.units) {
                unitList.add(UnitFactory.newUnit(unit.getUnitType(), unit.getName(), unit.getHealth()));
            }
            return unitList;
        }

    /**
     * Gets a random unit from the army
     * @return Returns a unit
     */
        public Unit getRandomUnit () {
            Random randomGenerator = new Random();
            return units.get(randomGenerator.nextInt(this.units.size()));
        }

    /**
     * Clears the army of all units
     */
        public void clear(){
            this.units.clear();
        }

    /**
     * Removes a list of units from the army
     * @param units List of units
     */
    public void removeAll(ArrayList<Unit> units){
            this.units.removeAll(units);
        }

    /**
     * Returns the total number of units
     * @return number of units: int
     */
    public int getTotalUnitsAmount(){
        return units.size();
    }

    /**
     * Returns the total number of units of a specific type
     * @param className The name of the unit type
     * @return the total number of that given class: int
     */
    public int getUnitsByClass(String className){
        int amount = 0;
        for (Unit unit : units) {
            if (unit.getClass().getSimpleName().equalsIgnoreCase(className)){
                amount += 1;
            }
        }
        return amount;
    }

    /**
     * toString method returns the some information about the army
     * @return Returns a string with information about the army.
     */
        @Override
        public String toString () {
            return "Army: " + name + ", has " + this.units.size() + " soldiers remaining.";
        }
    }

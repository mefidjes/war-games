package ntnu.idatt2001.wargames.Battle;
import ntnu.idatt2001.wargames.Listener.BattleListener;
import ntnu.idatt2001.wargames.Units.*;

import java.util.ArrayList;
import java.util.List;

public class Battle {

    public enum Terrain {
        HILL("Hill"),
        FOREST("Forest"),
        PLAINS("Plains");

        final String name;

        Terrain(String name){
            this.name = name;
        }

        public String getName(){
            return name;
        }

    }

    private static boolean shutdownRequest = false;
    private static Terrain battleTerrain = Terrain.PLAINS;



    private final List<BattleListener> listeners = new ArrayList<BattleListener>();

    Army armyOne;
    Army armyTwo;

    /**
     * Constructor takes in two armies as parameters and uses the getAllUnits method to create deep copies
     * @param armyOne
     * @param armyTwo
     */
    public Battle(Army armyOne, Army armyTwo) {
        try {
            this.armyOne = armyOne;
            this.armyTwo = armyTwo;
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Army can not be null");
        }
    }

    /**
     * Method returns the current state of the static battle terrain
     * @return Battle terrain
     */
    public static Terrain getBattleTerrain(){
        return battleTerrain;
    }

    /**
     * Sets the current battle terrain
     * @param terrain Takes in the desired terrain
     */
    public static void setBattleTerrain(Terrain terrain) {
        battleTerrain = terrain;
    }
    /**
     * Simulate method takes a random unit from each class and attacks a random unit from the other army.
     * After each attack it checks to see if the defending unit has 0 or less hp. If it does
     * it will update the battle listener and force the thread to sleep so that the unit count goes down
     * procedurally instead of instantly. Once either army is dead it will return the winning army.
     * @return Returns the winning army
     */
    public Army simulate() {
        while (armyOne.hasUnits() && armyTwo.hasUnits() && !shutdownRequest) {
            Unit soldierOne = armyOne.getRandomUnit();
            Unit soldierTwo = armyTwo.getRandomUnit();
            soldierOne.attack(soldierTwo);
            if (soldierTwo.getHealth() <= 0) {
                armyTwo.remove(soldierTwo);
                try{
                    update();
                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                if (!armyTwo.hasUnits()){
                    return armyOne;
                }
            }
            soldierOne = armyOne.getRandomUnit();
            soldierTwo = armyTwo.getRandomUnit();
            soldierTwo.attack(soldierOne);
            if (soldierOne.getHealth() <= 0) {
                armyOne.remove(soldierOne);
                try{
                    update();
                    Thread.sleep(25);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                if (!armyOne.hasUnits()){
                    return armyTwo;
                }
            }
        }
        return null;

    }

    /**
     * Simple method to force the listener to update on a specific condition. Prevents it from running all the time
     */
    public void update(){
        for (BattleListener listener : listeners) {
            listener.onUnitCountChange();
        }
    }

    /**
     * Method that lets other classes add a listener to the battle class
     * @param listener Takes in a listener
     */
    public void addListener(BattleListener listener){
        listeners.add(listener);
    }

    /**
     * Sets the boolean ShutdownRequest to true, prevents the thread from running when switching panel or on specific
     * condiditions.
     */
    public static void setShutdownRequest(){
        shutdownRequest = true;
    }

    /**
     * Converts the two armies to a String
     * @return A string of army1 vs army2
     */
    public String toString() {
        return armyOne.getName() + " vs. " + armyTwo.getName();
    }
}

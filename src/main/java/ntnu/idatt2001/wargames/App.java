package ntnu.idatt2001.wargames;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import ntnu.idatt2001.wargames.Scenes.SceneView;
import ntnu.idatt2001.wargames.Scenes.ViewSwitcher;

import java.io.IOException;


public class App extends Application {
    @Override
    public void start(Stage stage) throws IOException {

        Scene scene = new Scene(new Pane());
        ViewSwitcher.setScene(scene);
        ViewSwitcher.switchTo(SceneView.MAIN);
        stage.setScene(scene);
        stage.setTitle("War Games: The Simulator");
        stage.getIcons().add(new Image("file:src/main/resources/ntnu/idatt2001/wargames/images/peace.png"));
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
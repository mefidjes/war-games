package ntnu.idatt2001.wargames.Units;

import java.util.ArrayList;

import static ntnu.idatt2001.wargames.Units.UnitType.*;

public class UnitFactory {
    public static Unit newUnit(UnitType type, String unitName, int unitHealth){
        if (type == INFANTRY) {
            return new InfantryUnit(unitName, unitHealth);
        } else if (type == RANGED) {
            return new RangedUnit(unitName, unitHealth);
        } else if (type == CAVALRY) {
            return new CavalryUnit(unitName, unitHealth);
        } else if (type == COMMANDER) {
            return new CommanderUnit(unitName, unitHealth);
        }
        return null;
    }

    public static ArrayList<Unit> newUnits(UnitType type, String unitName, int unitHealth, int amount){
        ArrayList<Unit> units = new ArrayList<>();

        if (type == INFANTRY) {
            for (int i = 0; i < amount; i++){
                units.add(new InfantryUnit(unitName, unitHealth));
            }
        } else if (type == UnitType.RANGED) {
            for (int i = 0; i < amount; i++){
                units.add(new RangedUnit(unitName, unitHealth));
            }
        } else if (type == UnitType.COMMANDER) {
            for (int i = 0; i < amount; i++){
                units.add(new CommanderUnit(unitName, unitHealth));
            }
        } else if (type == UnitType.CAVALRY) {
            for (int i = 0; i < amount; i++){
                units.add(new CavalryUnit(unitName, unitHealth));
            }
        }
        return units;
    }

}

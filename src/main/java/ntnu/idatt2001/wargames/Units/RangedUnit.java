package ntnu.idatt2001.wargames.Units;

import ntnu.idatt2001.wargames.Battle.Battle;

public class RangedUnit extends Unit {

    UnitType unitType = UnitType.RANGED;

    int timesAttacked = 0;
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    @Override
    public UnitType getUnitType(){
        return unitType;
    }

    /**
     * Method for getting the terrain and applying a bonus depending on correct terrain
     * @return
     */
    @Override
    public int getAttackBonus() {
        return switch (Battle.getBattleTerrain()) {
            case HILL -> 7;
            case FOREST -> 5;
            default -> 3;
        };
    }

    /**
     * Returns a lowered defense for each time its hit the first and second time
     * @return
     */
    @Override
    public int getResistBonus() {

        if (timesAttacked == 0) {
            timesAttacked += 1;
            return 6;
        }
        else if (timesAttacked == 1) {
            timesAttacked += 1;
            return 4;
        }
        return 2;
    }



}

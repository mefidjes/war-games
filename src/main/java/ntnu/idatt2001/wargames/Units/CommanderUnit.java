package ntnu.idatt2001.wargames.Units;

import ntnu.idatt2001.wargames.Battle.Battle;

public class CommanderUnit extends CavalryUnit {
    /**
     * Subclass of the cavalryUnit, can block one hit and has a little stronger defense
     */
    boolean hasBeenAttacked = false;
    UnitType unitType = UnitType.COMMANDER;

    @Override
    public UnitType getUnitType(){
        return unitType;
    }

    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }

    @Override
    public int getResistBonus(){
        switch (Battle.getBattleTerrain()) {
            case FOREST:
                return 0;
            default:
                if (!hasBeenAttacked){
                    hasBeenAttacked = true;
                    return 100;
                } else {
                    return 4;
                }
        }
    }
}

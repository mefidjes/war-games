package ntnu.idatt2001.wargames.Units;

import ntnu.idatt2001.wargames.Battle.Battle;

public abstract class Unit {
    String name;
    int health;
    int attack;
    int armor;
    UnitType unitType;

    /**
     * Abstract class which acts as blueprint for other units
     * @param name Name of the unit
     * @param health Health of the unit
     * @param attack Attack of the unit
     * @param armor Armor of the unit
     */
    public Unit(String name, int health, int attack, int armor) {
        if (health <= 0) {
            throw new IllegalArgumentException("Health must be at least 1");
        }
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    public void attack(Unit opponent) {
        opponent.setHealth(Math.min(opponent.getHealth() - (this.attack + this.getAttackBonus()) + (opponent.getArmor() + opponent.getResistBonus()), opponent.getHealth()));
    }

    /**
     * Returns the name of the unit
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the unit
     * @param name: String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the health of the unit
     * @return health: int
     */
    public int getHealth() {
        return health;
    }

    /**
     * Sets the health of the unit
     * @param health: int
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Returns the attack of the unit
     * @return Returns attack of the unit
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Sets teh attack of the unit
     * @param attack: int
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * Returns the armor of the unit
     * @return int armor
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Sets the armor of the unit
     * @param armor: int
     */
    public void setArmor(int armor) {
        this.armor = armor;
    }

    public UnitType getUnitType(){
        return unitType;
    }

    /**
     * Returns the attack bonus of the unit
     * @return int attackBonus
     */
    public abstract int getAttackBonus();

    /**
     * Returns the resist bonus of the unit
     * @return resist bonus: int
     */
    public abstract int getResistBonus();

    /**
     * Returns the unit as a string
     * @return String
     */
    @Override
    public String toString(){
        return "Name: " + this.name + ", Health: " + this.health + ", Attack: " + this.attack + ", Armor: " + this.armor;
    }
}

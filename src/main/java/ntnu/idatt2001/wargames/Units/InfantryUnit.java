package ntnu.idatt2001.wargames.Units;

import ntnu.idatt2001.wargames.Battle.Battle;

public class InfantryUnit extends Unit {

    UnitType unitType = UnitType.INFANTRY;

    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    @Override
    public UnitType getUnitType(){
        return unitType;
    }

    @Override
    public int getAttackBonus() {
        switch (Battle.getBattleTerrain()) {
            case FOREST:
                return 4;
            default:
                return 2;
        }
    }

    @Override
    public int getResistBonus() {
        switch (Battle.getBattleTerrain()) {
            case FOREST:
                return 3;
            default:
                return 1;
        }
    }

}

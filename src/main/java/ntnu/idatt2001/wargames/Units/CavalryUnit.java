package ntnu.idatt2001.wargames.Units;

import ntnu.idatt2001.wargames.Battle.Battle;

public class CavalryUnit extends Unit {

    UnitType unitType = UnitType.CAVALRY;

    boolean hasAttacked = false;
    boolean hasBeenAttacked = false;

    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }


    @Override
    public UnitType getUnitType(){
        return unitType;
    }

    /**
     * Get attack bonus returns a highered the first time it attacks and reduced every consequent time
     * @return
     */
    @Override
    public int getAttackBonus() {
        if (!hasAttacked) {
            hasAttacked = true;
            return 6 + getTerrainAttackBonus();
        }
        else {
            return 2 + getTerrainAttackBonus();
        }
    }

    /**
     * If the cavalry unit has not been attacked it will have a higher resist bonus represented by it being mounted.
     * Once it has been attacked this bonus gets decreased.
     * @return
     */
    @Override
    public int getResistBonus() {
        switch (Battle.getBattleTerrain()) {
            case FOREST:
                return 0;
            default:
                if (!hasBeenAttacked){
                    hasBeenAttacked = true;
                    return 4;
                } else {
                    return 1;
                }
        }
    }

    /**
     * Returns the terrain attack bonus in different terrains
     * @return
     */
    private int getTerrainAttackBonus(){
        return switch (Battle.getBattleTerrain()) {
            case PLAINS -> 6;
            default -> 0;
        };
    }
}

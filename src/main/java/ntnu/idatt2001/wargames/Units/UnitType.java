package ntnu.idatt2001.wargames.Units;

public enum UnitType {
    INFANTRY,
    RANGED,
    CAVALRY,
    COMMANDER;

    UnitType(){
    }
}

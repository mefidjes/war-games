package ntnu.idatt2001.wargames.IO;

import ntnu.idatt2001.wargames.Battle.Army;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ArmyReaderTest {

    @Test
    void readArmyTest() {
        ArmyReader reader = new ArmyReader();
        Army testArmy = null;
        try {
            testArmy = reader.readArmyFromFile(new File("src/main/resources/ntnu/idatt2001/wargames/armies/testArmy.csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert testArmy != null;
        assertEquals("Test Army", testArmy.getName());
        assertEquals(36, testArmy.getAllUnits().size());
    }

    @Test
    void readNonExistentFile() {
        ArmyReader reader = new ArmyReader();
        assertThrows(IOException.class, () -> reader.readArmyFromFile(new File("src/main/resources/NonExistentFile.csv")));
    }

    @Test
    void readEmptyFile() {
        ArmyReader reader = new ArmyReader();
        assertThrows(IOException.class, () -> reader.readArmyFromFile(new File("src/main/resources/TestArmy2.csv")));
    }

    @Test
    void readWrongFileType() {
        ArmyReader reader = new ArmyReader();
        assertThrows(IOException.class, () -> reader.readArmyFromFile(new File("src/main/resources/WrongFileType.txt")));
    }

}
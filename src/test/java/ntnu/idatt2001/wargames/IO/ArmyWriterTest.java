package ntnu.idatt2001.wargames.IO;

import ntnu.idatt2001.wargames.Battle.Army;
import ntnu.idatt2001.wargames.Units.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

class ArmyWriterTest {

    @Test
    void writeToFile() throws IOException {

        Army testArmy = new Army("Test Army");
        testArmy.add(new CommanderUnit("Luke Skywalker", 30));
        for (int i = 0; i < 30; i++){
            testArmy.add(new InfantryUnit("Clone Trooper", 10));
        }
        for (int i = 0; i < 10; i++){
            testArmy.add(new RangedUnit("Laser Trooper", 5));
        }

        ArmyWriter.writeArmyToFile(testArmy, new File("src/main/resources/ntnu/idatt2001/wargames/armies/testArmy.csv"));

    }
}
package ntnu.idatt2001.wargames.Battle;

import ntnu.idatt2001.wargames.Units.InfantryUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {

    Army testArmy1 = new Army("TestArmy1");
    Army testArmy2 = new Army("TestArmy2");
    Battle testBattle = new Battle(testArmy1, testArmy2);



    void populate(Army army, String name, int number) {
        army.clear();
        for (int i = 0; i < number; i++) {
            army.add(new InfantryUnit(name, 5));
        }
    }

    @Test
    void simulateBattleWithCertainVictories() {
        populate(testArmy1, "Soldier1", 300);
        populate(testArmy2, "Soldier2", 10);
        assertTrue(testBattle.simulate() == testArmy1);
        populate(testArmy1, "Soldier1", 10);
        populate(testArmy2, "Soldier2", 300);
        assertTrue(testBattle.simulate() == testArmy2);


    }

    @Test
    void simulateBalancedTeams() {
        populate(testArmy1, "Soldier1", 10);
        populate(testArmy2, "Soldier2", 10);
        assertNotEquals(testBattle.simulate(), testBattle.simulate());
    }

    @Test
    void toStringTest() {
        assertEquals("TestArmy1 vs. TestArmy2", testBattle.toString());
    }
}
package ntnu.idatt2001.wargames.Battle;

import ntnu.idatt2001.wargames.Units.InfantryUnit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {

    Army testArmy = new Army("Test1");


    @Test
    void add() {
        assertNull(testArmy.getAllUnits());
        InfantryUnit testUnit = new InfantryUnit("bob", 10);
        testArmy.add(testUnit);
        assertEquals(testArmy.getRandomUnit(), testUnit);
    }


    @Test
    void remove() {
        InfantryUnit bob = new InfantryUnit("bob", 10);
        testArmy.add(bob);
        assertEquals(testArmy.getTotalUnitsAmount(), 1);
        testArmy.remove(bob);
        assertEquals(testArmy.getTotalUnitsAmount(), 0);
    }

    @Test
    void hasUnit() {
        InfantryUnit bob = new InfantryUnit("bob", 10);
        testArmy.add(bob);
        assertTrue(testArmy.hasUnit(bob));
    }

    @Test
    void hasUnits() {
        assertFalse(testArmy.hasUnits());
        testArmy.add(new InfantryUnit("Soldier", 2));
        assertTrue(testArmy.hasUnits());
    }

    /**
     * Creates two armies with where the second army takes a deep copy of the first army. By modifying a soldier in the first
     * army the second army should not be modified.
     */
    @Test
    void getAllUnits() {
        testArmy.add(new InfantryUnit("TestDummy", 100));
        Army testArmy2 = new Army("Test Army 2");
        testArmy2.addAll(testArmy.getAllUnits());

        assertTrue(testArmy.getRandomUnit() != testArmy2.getRandomUnit());
    }

    @Test
    void getRandomUnit() {
        testArmy.add(new InfantryUnit("beta", 10));
        testArmy.add(new InfantryUnit("charlie", 10));
        testArmy.add(new InfantryUnit("delta", 10));
        testArmy.add(new InfantryUnit("f", 10));

        assertFalse(testArmy.getRandomUnit() == testArmy.getRandomUnit() && testArmy.getRandomUnit() == testArmy.getRandomUnit());

    }

    @Test
    void testToString() {
        testArmy.add(new InfantryUnit("bob", 10));
        assertEquals(testArmy.toString(), "Army: Test1, has 1 soldiers remaining.");
    }
}
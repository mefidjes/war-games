package ntnu.idatt2001.wargames.Units;

import ntnu.idatt2001.wargames.Battle.Battle;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UnitTest {

    @Test
    @DisplayName ("Test for creating units with invalid hp")
    public void UnitTestInvalidHp(){
        assertThrows(IllegalArgumentException.class ,()->new InfantryUnit("", 0));
        assertThrows(IllegalArgumentException.class ,()->new InfantryUnit("", -50));
    }

    @Test
    @DisplayName("Test set health method")
    public void setHealthTest(){
        InfantryUnit infantryUnit = new InfantryUnit("InfantryUnitTest", 100);
        assertEquals(100, infantryUnit.getHealth());
        infantryUnit.setHealth(50);
        assertEquals(50, infantryUnit.getHealth());
        assertThrows(IllegalArgumentException.class, () -> infantryUnit.setHealth(0));
        assertThrows(IllegalArgumentException.class, () -> infantryUnit.setHealth(-1));

    }

    @Test
    @DisplayName ("Test for attack bonus")
    public void testCavalryAttackTest(){
        Unit cavalryTest = new CavalryUnit("TestCavalryUnit", 100);
        assertEquals(6, cavalryTest.getAttackBonus());
        assertEquals(2, cavalryTest.getAttackBonus());
        assertEquals(2, cavalryTest.getAttackBonus());

    }

    @Test
    @DisplayName ("Test for cavalry terrain bonus")
    public void testCavalryTerrainBonus() {
        Unit cavalryTest = new CavalryUnit("TestCavalryUnit", 100);
        assertEquals(4, cavalryTest.getResistBonus());
        Battle.setBattleTerrain(Battle.Terrain.FOREST);
        assertEquals(0, cavalryTest.getResistBonus());
    }

    @Test
    @DisplayName ("Test for cavalry terrain bonus")
    public void testRangedTerrainBonus() {
        Unit rangedUnit = new RangedUnit("TestCavalryUnit", 100);
        Battle.setBattleTerrain(Battle.Terrain.HILL);
        assertEquals(7, rangedUnit.getAttackBonus());
        Battle.setBattleTerrain(Battle.Terrain.FOREST);
        assertEquals(5, rangedUnit.getAttackBonus());
        Battle.setBattleTerrain(Battle.Terrain.PLAINS);
        assertEquals(3, rangedUnit.getAttackBonus());
    }

    @Test
    @DisplayName ("Test for cavalry terrain bonus")
    public void testInfantryTerrainBonus() {
        Unit infantryUnit = new InfantryUnit("Infantry Unit", 100);
        Battle.setBattleTerrain(Battle.Terrain.FOREST);
        assertEquals(4, infantryUnit.getAttackBonus());
        assertEquals(3, infantryUnit.getResistBonus());
        Battle.setBattleTerrain(Battle.Terrain.PLAINS);
        assertEquals(2, infantryUnit.getAttackBonus());
        assertEquals(1, infantryUnit.getResistBonus());    }



    @Test
    @DisplayName("Test to verify attacks will never heal enemy unit")
    public void attackSomeoneWithGreaterDefenseThanAttack(){
        Unit weakAttacker = new InfantryUnit("Weak Attacker", 100, 5, 0);
        Unit strongDefender = new InfantryUnit("Strong defender", 100, 0, 30);

        assertEquals(100, strongDefender.getHealth());
        weakAttacker.attack(strongDefender);
        assertEquals(100, strongDefender.getHealth());
    }

    @Test
    @DisplayName("Test resist bonus for archers")
    public void resistBonusTest(){
        RangedUnit rangedUnit = new RangedUnit("RangedUnitTest", 10);
        assertEquals(6, rangedUnit.getResistBonus());
        assertEquals(4, rangedUnit.getResistBonus());
        assertEquals(2, rangedUnit.getResistBonus());
        assertEquals(2, rangedUnit.getResistBonus());
    }
}